package br.com.generic.service.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.servlet.SparkApplication;

public class ServletSparkApplication implements SparkApplication{

    private static final Logger LOGGER = LoggerFactory.getLogger(ServletSparkApplication.class);

    @Override
    public void init() {
	LOGGER.info("init");
    }
}