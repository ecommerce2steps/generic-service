package br.com.generic.service.config;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jetty.JettyHttpComponent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class JettyComponentConfigurator extends RouteBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(JettyComponentConfigurator.class);

    @Value("${camel.jetty.component.maxThreads:150}")
    private int maxThreads;

    @Value("${camel.jetty.component.minThreads:150}")
    private int minThreads;

    @Value("${camel.jetty.component.queueSize:6000}")
    private int queueSize;

    @Value("${camel.jetty.component.connectors:3}")
    private int connectors;

    @Value("${camel.jetty.component.config.enabled:true}")
    private boolean enableThreadConfigs;

    @Override
    public void configure() throws Exception {
        LOGGER.info("Invoking configure(), values({})", StringUtils.join(new Object[]{}, ", "));
        LOGGER.info("enableThreadConfigs: {}", enableThreadConfigs);
        LOGGER.info("minThreads: {}", minThreads);
        LOGGER.info("maxThreads: {}", maxThreads);
        LOGGER.info("queueSize: {}", queueSize);
        if (enableThreadConfigs) {
            JettyHttpComponent jettyComponent = getContext().getComponent("jetty", JettyHttpComponent.class);
            LOGGER.info("Jetty - MaxThreads: {}", jettyComponent.getMaxThreads());
            LOGGER.info("Jetty - MinThreads: {}", jettyComponent.getMinThreads());

            jettyComponent.setMaxThreads(maxThreads);
            jettyComponent.setMinThreads(minThreads);

            Map<String, Object> connectorProperties = jettyComponent.getSocketConnectorProperties();
            if (connectorProperties == null) connectorProperties = new HashMap<>();
            connectorProperties.put("acceptors", connectors);
            connectorProperties.put("statsOn", "false");
            connectorProperties.put("soLingerTime", "5000");
            jettyComponent.setSocketConnectorProperties(connectorProperties);

            showProperties("jettyComponent.getSocketConnectorProperties", jettyComponent.getSocketConnectorProperties());

            LOGGER.info("Jetty - MaxThreads: {}", jettyComponent.getMaxThreads());
            LOGGER.info("Jetty - MinThreads: {}", jettyComponent.getMinThreads());
        }
    }

    private void showProperties(String name, Map properties) {
        LOGGER.info("Properties of: {}", name);
        properties.forEach((key, value) -> {
            LOGGER.info("- {}={}", key, value);
        });
    }
}