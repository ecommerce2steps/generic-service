package br.com.generic.service.config;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import spark.servlet.SparkFilter;

import java.net.InetAddress;

@Component
public class RestComponentConfiguration extends RouteBuilder{

    private static final Logger LOGGER = LoggerFactory.getLogger(RestComponentConfiguration.class);

    @Value("${spark.port:7001}")
    private int port;

    @Value("${server.port:8080}")
    private Integer serverPort;

    @Value("${host.name:}")
    private String preferableNetworkName;

    @Value("${host.port:}")
    private Integer preferableNetworkPort;

    @Override
    public void configure() throws Exception {
        LOGGER.info("### Configuring Spark Component: {} ###", port);
        String hostname = getPreferableNetwork();
        String hostAddress = hostname + ":" + port;
        LOGGER.info("Host: {}", hostAddress);
        Integer apiPort = preferableNetworkPort == null ? serverPort : preferableNetworkPort;
        restConfiguration().component("spark-rest").enableCORS(true)
                .bindingMode(RestBindingMode.json).jsonDataFormat("json-jackson")
                .dataFormatProperty("prettyPrint", "true")
                .apiContextPath("/api-doc")
                .apiProperty("api.title", "Product Metrics").apiProperty("api.version", "1.1")
                .apiProperty("cors", "true")
                .apiProperty("host", hostname + ":" + apiPort)
                .port(port);
    }

    public String getPreferableNetwork() throws Exception {
        LOGGER.info("GETTING getPreferableNetwork: {}", preferableNetworkName);
        String hostName = null;
        if (StringUtils.isEmpty(preferableNetworkName)) {
            hostName = InetAddress.getLocalHost().getHostAddress();
        } else {
            hostName = preferableNetworkName;
        }

        LOGGER.info("Returned hostName: {}", hostName);
        return hostName;
    }

    @Bean
    public FilterRegistrationBean myFilterForSparkIntegration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new SparkFilter());
	registration.addInitParameter("applicationClass", "br.com.generic.service.config.ServletSparkApplication");

        registration.addUrlPatterns("/api-doc/*");
        registration.addUrlPatterns("/ws/*");

        LOGGER.info(">>> Registering Spark filter");
        return registration;
    }
}