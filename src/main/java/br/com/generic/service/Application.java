package br.com.generic.service;

import br.com.ecomm.global.components.rules.SpringRuleDependencyLoaderImpl;
import br.com.ecomm.global.components.rules.executor.RuleDependencyLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;


@SpringBootApplication(scanBasePackages = "br.com.generic.service")
public class Application {


    @Autowired
    private ApplicationContext applicationContext;

    @Bean
    @Qualifier(value = "logger")
    public Logger logger(){
        return LoggerFactory.getLogger(Application.class);
    }

    @Bean
    public RuleDependencyLoader loaderDependency(){
       return new SpringRuleDependencyLoaderImpl(applicationContext);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
